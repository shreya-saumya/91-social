import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {CardContent, List} from "@material-ui/core";
import Card from "@material-ui/core/Card";
class CardComponent extends Component {
    constructor(props) {
        super(props);

    }
    componentDidMount() {
        console.log('props: ',this.props.propsLink);
    }

    render() {
        return (
            <div>
                <Card style={{margin:'30px'}} variant={"outlined"}>
                    <CardContent >
                        {this.props.propsValue}
                    </CardContent>
                </Card>
            </div>
        );
    }
}

CardComponent.propTypes = {};

export default CardComponent;
