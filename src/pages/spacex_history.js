import React, {Component} from 'react';
import axios from "axios"
import CardComponent from "../components/cardComponent";
import {CardContent, List} from "@material-ui/core";
import spacexlogo from "../static/images/spacexlogo.jpg";
import ListItem from "@material-ui/core/ListItem";
import {fetchHistory} from "../redux";
import {connect} from 'react-redux'
import ListItemIcon from "@material-ui/core/ListItemIcon";
import {CallMade, ExpandLess, ExpandMore, StarBorder} from "@material-ui/icons";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import InputBase from "@material-ui/core/InputBase";
var response=[];
var DataPerPage= 5;

class SpacexHistory extends Component {
    constructor(props) {
        super(props);
        this.state={
            currentPage:1,
            open:false,
            date:"",
            query:""
        }
        this.handleClick = this.handleClick.bind(this);
    }
    handleOpenLink = () => {
        if(this.state.open===true){
            this.setState({
                open:false
            })
        }
        if (this.state.open===false){
            this.setState({
                open:true
            })
        }
    };


    handleClick =(event)=> {
        console.log(response);
        // currentPage=Number(event.target.id);
        this.setState({
            currentPage: Number(event.target.id)
        });
    }

    componentDidMount() {
        axios.get('https://api.spacexdata.com/v3/history').then(res=>{
                for (let i=0;i<res.data.length;i++){
                    console.log('response' ,res.data[i]);
                    response[i]=res.data[i];
                    this.setState({
                        date:res.data[i].event_date_utc
                    })
                }

    })


}
handleChange=(event)=>{
        this.setState({
            query:event.target.value
        })
    console.log("query", this.state.query);


    if( this.state.query.length<0|| this.state.query===""){
        response=response;
        return response
    }
    else if (response.length>0){
        response= response.filter((q)=>{
            return  q.title.toLowerCase().includes(this.state.query.toLowerCase());
        })
    }
    else if (response.filter((q)=>{
        return q.title.toLowerCase()!==(this.state.query);
    })){
        return []
    }
   else {
        return response
     }

}

    render() {
        const { currentPage } = this.state;
        const indexOfLastData = currentPage * DataPerPage;
        const indexOfFirstData = indexOfLastData - DataPerPage;
        const currentTodos = response.slice(indexOfFirstData, indexOfLastData);

        const renderTodos = currentTodos.map((history, index) => {
            return (
                <CardComponent propsValue={
                    <div >
                         <span>
                        <img alt="SpaceXLogo" src={spacexlogo} style={{verticalAlign: 'middle',
                            width: '40px',
                            height: '40px',
                            borderRadius: '50%'}}/><b >{history.title}</b>
                            <span style={{color:'grey',padding:'5px'}}>| {new Date(this.state.date).getDate() } -
                            {new Date(this.state.date).getMonth()} -
                                {new Date(this.state.date).getFullYear()}
                            </span>
                        </span>
                        {/*<div>ID: {this.props.propsId}</div>*/}
                        <div style={{margin:'20px'}}> {history.details}</div>
                        <hr/>

                        <div>
                            <List  >
                                <ListItem button onClick={this.handleOpenLink}>
                                    <ListItemIcon>
                                        <CallMade></CallMade>
                                    </ListItemIcon>
                                    <ListItemText primary="Find out more on relevant links:" />
                                    {this.state.open ? <ExpandLess /> : <ExpandMore />}
                                </ListItem>
                                <Collapse in={this.state.open} timeout="auto" unmountOnExit>
                                    <List component="div" disablePadding>
                                        <a style={{textDecoration:'none', color:'black'}} href={history.links.article}>
                                            <ListItem button >
                                                <ListItemIcon>
                                                    <StarBorder />
                                                </ListItemIcon>
                                                <ListItemText primary="articles" />
                                            </ListItem>
                                        </a>
                                        {history.links.reddit?<a style={{textDecoration:'none', color:'black'}} href={history.links.reddit}>
                                            <ListItem button >
                                                <ListItemIcon>
                                                    <StarBorder />
                                                </ListItemIcon>
                                                <ListItemText primary="reddit" />
                                            </ListItem>
                                        </a>:''}

                                    </List>
                                </Collapse>
                            </List>
                        </div>
                        {/*articles:  <a >{this.props.propsLink.article}</a>*/}
                        {/*{this.props.propsLink.reddit?<div>reddit: <a href={this.props.propsLink.reddit}>{this.props.propsLink.reddit}</a></div>:''*/}
                        {/*}*/}
                    </div>
                }
                >
                </CardComponent>
            );
        });


        const pageNumbers = [];
        for (let i = 1; i <= Math.ceil(response.length / DataPerPage); i++) {
            pageNumbers.push(i);
        }

        const renderPageNumbers = pageNumbers.map(number => {
            return (

                <span>
                    <button  style={{width:'50px'}} key={number}
                            id={number}
                            onClick={this.handleClick}>
                        {number}
                    </button>

                </span>
            );
        });

        return (
            <div>
                <input  type={"text"} placeholder={"search..."} style={{width:'200px',height:'30px',
                    color:'white',margin:'20px',background:'#262626'}} onChange={this.handleChange}/>

                <ul>
                    {renderTodos}
                </ul>
                <ul id="page-numbers">
                    page no:{renderPageNumbers}
                </ul>
            </div>
        );
    }
}

const mapStateToProps = (state) =>{
return{
    historyData:state.historyResponse
   }
}

const mapDispatchToProps=(dispatch)=>{
    return{
        fetchHistory:()=>dispatch(fetchHistory())
    }
}

export default connect(mapStateToProps,mapDispatchToProps)(SpacexHistory);
